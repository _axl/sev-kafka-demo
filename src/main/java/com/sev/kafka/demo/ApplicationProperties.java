package com.sev.kafka.demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {

    private Properties appProps = new Properties();

    public ApplicationProperties() throws IOException {
        final String rootPath = Thread.currentThread().
                getContextClassLoader().
                getResource("").
                getPath();
        final String appConfigPath = rootPath + "application.properties";

        appProps.load(new FileInputStream(appConfigPath));
    }

    public String getBootstrapServers() {
        return this.appProps.getProperty("bootstrap.url");
    }

    public String getRegistryUrl() {
        return this.appProps.getProperty("registry.url");
    }

    public String getTopicName() {
        return this.appProps.getProperty("topic.name");
    }

    public String getParquetPath() {
        return this.appProps.getProperty("output.path");
    }

    public Integer getWindowSize() {
        return Integer.valueOf(this.appProps.getProperty("window.size"));
    }
}
