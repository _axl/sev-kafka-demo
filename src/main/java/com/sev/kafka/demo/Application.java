package com.sev.kafka.demo;

import java.io.IOException;

public class Application {

	public static void main(String[] args) {
        final ApplicationProperties properties;
	    try {
            properties = new ApplicationProperties();
        } catch (IOException e) {
	        throw new RuntimeException("Could not read props file", e);
        }

		final FluxConsumer consumer = new FluxConsumer(properties);
        consumer.start();

		final FLuxProducer producer = new FLuxProducer(properties);
		producer.start();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {producer.stop();} catch (Throwable e) {}
                try {consumer.stop();} catch (Throwable e) {}
            }
        });
	}
}
