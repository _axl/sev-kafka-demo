package com.sev.kafka.demo;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.avro.generic.GenericRecord;
import org.apache.hadoop.fs.Path;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.parquet.avro.AvroParquetWriter;
import org.apache.parquet.hadoop.ParquetWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class FluxConsumer {

    private static final Logger logger = LoggerFactory.getLogger(FluxConsumer.class);

    private ApplicationProperties properties;
    private ParquetWriter<TimeTick> writer;
    private ReceiverOptions<String, GenericRecord> receiverOptions;
    private Flux<ReceiverRecord<String, GenericRecord>> inboundFlux;

    public FluxConsumer(ApplicationProperties properties) {
        this.properties = properties;
        final Path outputPath = new Path(properties.getParquetPath()
                + "timetick-parq-" + System.currentTimeMillis());
        try {
            writer = AvroParquetWriter.
                    <TimeTick>builder(outputPath).
                    withSchema(TimeTick.SCHEMA$).
                    build();
        } catch (IOException e) {
            throw new RuntimeException("Could not open path for parquet", e);
        }
    }

    public void start() {
        try {
            new ConsumeTask().start();
        } catch (Throwable e) {
            logger.error("producer error", e);
        }
    }

    public void stop() {
        logger.info("stopping consumer");
        try {
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing parquet writer", e);
        }
    }

    // since consumer blocks the tread start new one
    private class ConsumeTask extends Thread {

        public void run() {
            try {
                doRun();
            } catch (Throwable e) {
                logger.error("error starting consuming", e);
            }
        }

        private void doRun() {
            logger.info("starting consumer");

            final Map<String, Object> props = new HashMap<>();
            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers());
            props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
            props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
            props.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer-group");
            props.put("schema.registry.url", properties.getRegistryUrl());

            final AtomicInteger total = new AtomicInteger();
            final long[] windowThreshold = new long[] {-1};

            receiverOptions = ReceiverOptions.<String, GenericRecord>create(props).
                    subscription(Collections.singleton(properties.getTopicName()));

            inboundFlux = KafkaReceiver.create(receiverOptions).receive();

            inboundFlux.
                    windowUntil(r -> {
                        long ts = (long) r.value().get("eventTs");
                        if (windowThreshold[0] == -1) {
                            windowThreshold[0] = ts + properties.getWindowSize();
                        }
                        if (ts > windowThreshold[0]) {
                            windowThreshold[0] = -1;
                            return true;
                        }
                        return false;
                    }).
                    map(w ->  {
                        logger.info("starting process new window");
                        return w;
                    }).
                    flatMap(window ->
                            window.map(r -> TimeTick.newBuilder().
                                    setEventTs((long)r.value().get("eventTs")).
                                    setValue((int) r.value().get("value")).
                                    build()).
                                    doOnNext(r -> {
                                        try {
                                            writer.write(r);
                                            total.incrementAndGet();
                                            if (total.intValue() % 1000 == 0) {
                                                logger.info("total records consumed={}", total.intValue());
                                            }
                                        } catch (IOException e) {
                                            logger.error("error writing record", e);
                                        }
                                    })).
                    blockLast();
//
//
//                flatMap(window ->
//                        window.reduce(new int[] {0, 0}, (a, b) -> {
//                            a[0]++;
//                            a[1] += (Integer) b.value().get("value");
//                            return a;
//                        }).
//                        doOnNext(a -> logger.info("record received={} sumValue={}", a[0], a[1]))).
//                blockLast();
        }
    }
}
