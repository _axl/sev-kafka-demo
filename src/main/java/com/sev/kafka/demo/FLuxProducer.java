package com.sev.kafka.demo;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class FLuxProducer {

    private static final Logger logger = LoggerFactory.getLogger(FLuxProducer.class.getName());

    private ApplicationProperties properties;
    private KafkaSender<String, TimeTick> sender;
    private ScheduledThreadPoolExecutor executor;
    private Random r = new Random();

    public FLuxProducer(ApplicationProperties properties) {
        this.properties = properties;
        executor = new ScheduledThreadPoolExecutor(10);
    }

    public void start() {
        try {
            doStart();
        } catch (Throwable e) {
            logger.error("consumer error", e);
        }
    }

    public void stop() {
        logger.info("stopping producer");
        try {
            sender.close();
        } catch (Exception e) {
            logger.error("error closing kafka sender", e);
        }
    }

    private void doStart() {
        logger.info("starting producer");

        final Map<String, Object> configProps = new HashMap<>();
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers());
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        configProps.put("schema.registry.url", properties.getRegistryUrl());

        final SenderOptions<String, TimeTick> senderOptions = SenderOptions.create(configProps);

        sender = KafkaSender.create(senderOptions);

        executor.scheduleAtFixedRate(this::sendMessages, 0, 500L, TimeUnit.MILLISECONDS);
    }

    private void sendMessages() {
        try {
            int send = r.nextInt(999);
            int v = _send(send);
            logger.info("total produced={} valSum={}", send, v);
        } catch (Exception e) {
            logger.error("error sending messages", e);
        }
    }

    private int _send(int send) {
        final CountDownLatch latch = new CountDownLatch(send);
        final AtomicInteger count = new AtomicInteger();
        sender.<Integer>send(Flux.range(1, send)
                .map(i -> {
                    final long ts = System.currentTimeMillis();
                    TimeTick timeTick = TimeTick.newBuilder().
                            setEventTs(ts).
                            setValue(r.nextInt(100)).
                            build();
                    count.addAndGet(timeTick.getValue());
                    latch.countDown();
                    return SenderRecord.
                            create(new ProducerRecord<>(
                                            this.properties.getTopicName(),
                                            String.valueOf(ts) + i,
                                            timeTick), i);
                }))
                .doOnError(e -> logger.error("send record failed", e))
                .subscribe(r -> {
                    //RecordMetadata metadata = r.recordMetadata();
                });

        try {
            latch.await();
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        return count.intValue();
    }
}
